package api.model;

import lombok.Data;

@Data
public class PublicEntry {
    private String api;
    private String description;
    private String auth;
    private Boolean https;
    private String cors;
    private String link;
    private String category;
}
