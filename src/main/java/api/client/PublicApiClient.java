package api.client;

import api.model.PublicEntry;
import io.restassured.RestAssured;

import java.util.List;

public class PublicApiClient {
    private static final String URL = "https://api.publicapis.org/entries";

    public List<PublicEntry> getEntries() {

        return RestAssured.when()
                .get(URL)
                .then()
                .statusCode(200).extract().jsonPath()
                .getList("entries", PublicEntry.class);
    }
}
