package ui.page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$x;

public class GoogleResult {
    private final ElementsCollection internalLinks = $$x("//div/div[1]/div/a/h3[not(ancestor::ul)]");

    public List<String> getInternalLinks() {
        return internalLinks.filter(Condition.visible).texts();
    }
}
