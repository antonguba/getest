package ui.page;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.$$x;

public class GooglePage {
    private final SelenideElement searchField = $(byName("q"));
    private final ElementsCollection acceptCookiesBtn = $$x("//button/div[@role='none']");

    public GooglePage acceptCookies() {
        Selenide.sleep(200);
        if ($("[role='dialog']").isDisplayed()) {
            acceptCookiesBtn.last().click();
        }
        return this;
    }
    public void searchFor(String text) {
        searchField.val(text).pressEnter();
    }
}
