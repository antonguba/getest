package ui;

import com.codeborne.selenide.Selenide;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.SoftAssertions;
import org.testng.annotations.Test;
import ui.page.GooglePage;
import ui.page.GoogleResult;

import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@Slf4j
public class SearchTest {
    private static final String URL = "https://www.google.co.il/";

    @Test(description = "Print google search result links")
    public void googleSearchLinksTest() {
        Selenide.open(URL);
        new GooglePage().acceptCookies().searchFor("Java");
        List<String> links = new GoogleResult().getInternalLinks();
        log.info("Internal links: {}", links);
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(links.get(0)).as("First link contains Java").containsIgnoringCase("java");
            softly.assertThat(links.get(links.size() - 1)).as("Last link doesn't contains interview").doesNotContainIgnoringCase("interview");
        });
    }
}
