package api;

import api.client.PublicApiClient;
import api.model.PublicEntry;
import lombok.extern.slf4j.Slf4j;

import org.testng.annotations.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@Slf4j
public class PublicApiTest extends BaseTest {
    private final PublicApiClient publicApiClient = new PublicApiClient();

    @Test(description = "Compare, count and verify the number of objects where the property appears")
    public void authCategoryTest() {
        List<PublicEntry> actualResults = publicApiClient.getEntries().stream()
                .filter(o -> o.getCategory().equals("Authentication & Authorization"))
                .collect(Collectors.toList());
        log.info("Category results: {}", actualResults);
        List<String> apis = actualResults.stream().map(PublicEntry::getApi).collect(Collectors.toList());
        assertThat(apis).containsExactlyInAnyOrder(
                "Auth0", "GetOTP", "Micro User Service",
                "MojoAuth", "SAWO Labs", "Stytch", "Warrant");
    }

}
